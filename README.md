[ESP_mock]
======================

ESP_mock is a simple NodeJS application that imitate ESP8266 behave for
the [Crouton](http://crouton.mybluemix.net/crouton/gettingStarted) dashboard.

Running the application:
--------------
```
    $ git clone git@bitbucket.org:Hastler/esp_mock.git
    $ cd esp_mock
    $ npm install
    $ npm start
```

Getting Started
-------
To start you need a **MQTT Broker** and **Crouton Dashboard**.