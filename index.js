/**
 * Created by andr on 05.08.16.
 */
'use strict';

let
  mqtt = require('mqtt'),
  client = mqtt.connect('mqtt://127.0.0.1:1883'),
  baseDevice = require('./devices/base_device'),

  SimpleText = require('./sensors/simpleText'),
  SimpleInput = require('./sensors/simpleInput'),
  SimpleSlider = require('./sensors/simpleSlider'),

  tSensor = new SimpleText({
    name: 'tSensor',
    units: 'degree',
    title: 'Bathroom Temp'
  }),

  greetingsPanel = new SimpleInput({
    name: 'greetingsPanel',
    title: 'Greetings'
  }),

  mainTemperature = new SimpleSlider({
    name: 'mainTemperature',
    title: 'Main Temperature',
    units: 'degrees',
    min: 5,
    max: 32
  }),

  device = new baseDevice({
    name: 'ESP10',
    description: 'Javascript ESP emulation'
  }, client);


device.addSensor(tSensor);
//device.setSensorValues('tSensor', { value: 44 });

device.addSensor(greetingsPanel);
device.setSensorValues('greetingsPanel', { value: 'Welcome home, Andrey!'});

device.addSensor(mainTemperature);
device.setSensorValues('mainTemperature', { value: 22 });

client.on('connect', () => {
  console.log('Connected to MQTT-broker, registering devices');
  device.connect();
  device.subscribe();
});


client.on('message', (channel, message) => {
  let
    sections = channel.split('/');

  device.processMessage(sections.pop(), message);
});

client.on('error', err => console.log(err));


// Temperature will be trying to reach slider value (just for fun)
let
  temperature = 23; // base temperature

setInterval(() => {
  let
    sliderValue = device.getSensorValues('mainTemperature').value;

  temperature = sliderValue > temperature ? temperature + 1 : temperature - 1;

  device.setSensorValues('tSensor', {value: temperature});
}, 3500);