/**
 * Created by andr on 06.08.16.
 */
'use strict';

class baseDevice {
  constructor(device, client) {
    this.client = client;
    this.name = device.name;
    this.description = device.description;
    this.sensors = {};
  }

  addSensor(sensor) {
    this.sensors[sensor.name] = sensor.endPoint;
    if (sensor.canReceiveValues) {
      this.client.subscribe(`/inbox/${this.name}/${sensor.name}`);
    }
  }

  setSensorValues(name, values) {
    if (this.sensors[name]) {
      this.sensors[name].values = values;
      this.sendSensorValues(name);
    }
  }

  getSensorValues(name) {
    return this.sensors[name] && this.sensors[name].values;
  }


  sendSensorValues(name) {
    this.client.publish(`/outbox/${this.name}/${name}`,
      JSON.stringify(this.getSensorValues(name)));
  }

  get deviceInfo() {
    return JSON.stringify({
      "deviceInfo": {
        "name": this.name,
        "endPoints": this.sensors,
        "description": this.description,
        "status": this.status
      }
    });
  }

  get status() {
    return "good";
  }

  publishDeviceInfo() {
    this.client.publish(`/outbox/${this.name}/deviceInfo`, this.deviceInfo);
  }

  lwt() {
    this.client.publish(`/outbox/${this.name}/lwt`, 'nothing');
  }

  connect() {
    this.publishDeviceInfo();
    this.lwt();
  }

  subscribe() {
    this.client.subscribe(`/inbox/${this.name}/deviceInfo`);
  }

  processMessage(endPoint, message) {
    console.log('Process endpoint', endPoint, message.toString());

    if (endPoint === 'deviceInfo') {
      this.publishDeviceInfo();
    } else {
      // TODO move parse to try...catch
      this.setSensorValues(endPoint, JSON.parse(message.toString()));
      this.sendSensorValues(endPoint);
    }
  }

}

module.exports = baseDevice;