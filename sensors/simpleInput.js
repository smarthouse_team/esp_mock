/**
 * Created by andr on 06.08.16.
 */
'use strict';

let
  Sensor = require('./sensor');

class SimpleInput extends Sensor {
  constructor(sensor) {
    super(sensor);
    this['card-type'] = 'crouton-simple-input';
    this.canReceiveValues = true;
  }

  get endPoint() {
    return {
      "card-type": this['card-type'],
      title: this.title,
      values: this.values
    }
  }
}

module.exports = SimpleInput;