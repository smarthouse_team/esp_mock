/**
 * Created by andr on 06.08.16.
 */
'use strict';

let
  Sensor = require('./sensor');

class SimpleSlider extends Sensor {
  constructor(sensor) {
    super(sensor);
    this.min = sensor.min;
    this.max = sensor.max;
    this['card-type'] = 'crouton-simple-slider';
    this.canReceiveValues = true;
  }

  get endPoint() {
    return {
      "card-type": this['card-type'],
      title: this.title,
      values: this.values,
      units: this.units,
      min: this.min,
      max: this.max
    }
  }
}

module.exports = SimpleSlider;