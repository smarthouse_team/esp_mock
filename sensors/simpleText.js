/**
 * Created by andr on 06.08.16.
 */
'use strict';

let
  Sensor = require('./sensor');

class SimpleText extends Sensor {
  constructor(sensor) {
    super(sensor);
    this['card-type'] = 'crouton-simple-text';
  }

  get endPoint() {
    return {
      units: this.units,
      "card-type": this['card-type'],
      title: this.title,
      values: this.values
    }
  }
}

module.exports = SimpleText;